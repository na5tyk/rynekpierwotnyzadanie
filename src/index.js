import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { Container } from "react-bootstrap";

import HomeView from "./views/HomeView";
import WeatherView from "./views/WeatherView";
import NotFound from "./views/NotFound";

import SearchWeatherForm from "./components/SearchWeatherForm";

import routes from "./utils/routes";

import store from "./store";

import reportWebVitals from "./reportWebVitals";

import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Container>
        <Router>
          <SearchWeatherForm />
          <Switch>
            <Route exact path={routes.homeView} component={HomeView} />
            <Route exact path={routes.weatherView} component={WeatherView} />
            <Route path={routes.notFound} component={NotFound} />
          </Switch>
        </Router>
      </Container>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
