import React from "react";
import { shallow } from "enzyme";

import HomeView from "./index";
const component = shallow(<HomeView />);

describe("HomeView", () => {
  it("should render without error", () => {
    expect(component).toBeTruthy();
  });

  it("snapshot", () => {
    expect(component).toMatchSnapshot();
  });
});
