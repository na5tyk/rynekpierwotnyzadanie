import React from "react";
import { shallow } from "enzyme";

import NotFound from "./index";
const component = shallow(<NotFound />);

describe("NotFound", () => {
  it("should render without error", () => {
    expect(component).toBeTruthy();
  });

  it("snapshot", () => {
    expect(component).toMatchSnapshot();
  });

  it("section should have correct text", () => {
    const section = component.find('section')
    expect(section.text()).toBe('Brak strony');
  });
});
