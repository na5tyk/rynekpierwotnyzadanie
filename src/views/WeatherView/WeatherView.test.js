import React from "react";
import { useSelector } from "react-redux";

import { shallow } from "enzyme";

import { weatherMock } from "../../test_mocks/weatherMocks";

import WeatherView from "./index";

const state = {
  weather: {
    weather: weatherMock,
    fetchingWeather: false,
  },
};

const stateNoWeather = {
  weather: {
    weather: null,
    fetchingWeather: false,
  },
};

const stateGetting = {
  weather: {
    weather: weatherMock,
    fetchingWeather: true,
  },
};

const mockDispatch = jest.fn();
const mockHistoryPush = jest.fn();

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
  useParams: () => jest.fn(),
}));

jest.spyOn(React, "useEffect").mockImplementation((f) => f());

let component;
const setup = (state) => {
  component?.unmount();
  useSelector.mockImplementation((selectFn) => selectFn(state));
  component = shallow(<WeatherView />);
};
setup(state);

describe("WeatherView", () => {
  test("snapshot", () => {
    expect(component).toMatchSnapshot();
  });

  test("should render 4 columns", () => {
    const columns = component.find("Col");
    expect(columns.length).toBe(4);
  });

  test("should render CompareWeather", () => {
    const compareWeather = component.find("CompareWeather");
    expect(compareWeather.length).toBe(1);
  });

  test("should render info about no data", () => {
    setup(stateNoWeather);
    const col = component.find("Col");
    expect(col.text()).toBe("Brak danych");
  });

  test("should render Spinner", () => {
    setup(stateGetting);
    const spinner = component.find("Spinner");
    expect(spinner.length).toBe(1);
  });
});
