import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import { Row, Col, Spinner, Card } from "react-bootstrap";

import { getWeather } from "../../store/actions/weatherActions";
import {
  getFetchedWeather,
  getFetchingWeather,
} from "../../store/selectors/weatherSelectors";
import CompareWeather from "../../components/CompareWeather";

const WeatherView = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const weather = useSelector((state) => {
    return getFetchedWeather(state);
  });

  const fetchingWeather = useSelector((state) => {
    return getFetchingWeather(state);
  });

  React.useEffect(() => {
    dispatch(getWeather(params.city, true));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params.city]);

  const convertTemp = (temp) => {
    if (!temp) return "-";
    temp = temp - 273.15;
    return `${temp.toFixed(2)} °C`;
  };

  if (fetchingWeather)
    return (
      <Card>
        <Row>
          <Col className="text-center">
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </Col>
        </Row>
      </Card>
    );

  if (!weather) {
    return (
      <Card>
        <Row>
          <Col className="text-center">Brak danych</Col>
        </Row>
      </Card>
    );
  }

  return (
    <>
      <Card>
        <Row>
          <Col lg={3}>
            <p className="text-center">Miasto:</p>{" "}
            <p className="text-center">{weather?.name ?? "-"}</p>
          </Col>
          <Col lg={3}>
            <p className="text-center">Temperatura:</p>{" "}
            <p className="text-center">{convertTemp(weather?.main?.temp)}</p>
          </Col>
          <Col lg={3}>
            <p className="text-center">Prędkość wiatru:</p>
            <p className="text-center">
              {weather?.wind?.speed ? `${weather.wind.speed} km/h` : null}
            </p>
          </Col>
          <Col lg={3}>
            <p className="text-center">Wilogtność: </p>
            <p className="text-center">
              {weather?.main?.humidity ? `${weather.main.humidity}%` : null}
            </p>
          </Col>
        </Row>
      </Card>
      <CompareWeather city={weather} />
    </>
  );
};

export default WeatherView;
