import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducers from "./reducers";

import { createLogger } from "redux-logger";

const logger = createLogger({
  collapsed: true,
  duration: true,
  predicate: (getState, action) => action.type !== "AUTH_REMOVE_TOKEN",
  stateTransformer: (state) => {
    const newState = {};

    for (var i of Object.keys(state)) {
      newState[i] = state[i];
    }

    return newState;
  },
});
const middlewares = [thunk, logger];
export default createStore(reducers, compose(applyMiddleware(...middlewares)));
