export const getFetchedWeather = (state) => state.weather.weather;
export const getFetchingWeather = (state) => state.weather.fetchingWeather;
