import {
  GET_WEATHER_FAILED,
  GET_WEATHER_REQUEST,
  GET_WEATHER_SUCCESS,
} from "../constants/weather";

const initialState = {
  fetchingWeather: false,
  weather: null,
};

const weather = (state = initialState, action) => {
  switch (action.type) {
    case GET_WEATHER_REQUEST:
      return {
        ...state,
        fetchingWeather: true,
      };
    case GET_WEATHER_SUCCESS:
      const stateToSave = {
        fetchingWeather: false,
        weather: action.payload,
      };

      if (!action.payload) {
        delete stateToSave.weather;
      }
      return stateToSave;
    case GET_WEATHER_FAILED:
      return {
        ...state,
        fetchingWeather: false,
        weather: null,
      };
    default:
      return state;
  }
};

export default weather;
