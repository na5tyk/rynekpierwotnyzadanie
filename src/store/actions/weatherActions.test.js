import mockAxios from "axios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import {
  GET_WEATHER_FAILED,
  GET_WEATHER_REQUEST,
  GET_WEATHER_SUCCESS,
  GET_WEATHER_WITHOUT_SAVE_SUCCESS,
} from "../constants/weather";
import {
  getWeather,
  getWeatherFailed,
  getWeatherRequest,
  getWeatherSuccess,
  getWeatherSuccessWithSave,
} from "./weatherActions";

import { weatherMock } from "../../test_mocks/weatherMocks";

jest.mock("axios");

describe("weatherActions", () => {
  describe("sync actions", () => {
    describe("getWeather", () => {
      it("getWeatherRequest", () => {
        const exceptedObject = {
          type: GET_WEATHER_REQUEST,
        };

        expect(getWeatherRequest()).toEqual(exceptedObject);
      });

      it("getWeatherSuccess", () => {
        const exceptedObject = {
          type: GET_WEATHER_WITHOUT_SAVE_SUCCESS,
        };

        expect(getWeatherSuccess()).toEqual(exceptedObject);
      });

      it("getWeatherSuccessWithSave", () => {
        const exceptedObject = {
          type: GET_WEATHER_SUCCESS,
          payload: weatherMock,
        };

        expect(getWeatherSuccessWithSave(weatherMock)).toEqual(exceptedObject);
      });

      it("getWeatherFailed", () => {
        const exceptedObject = {
          type: GET_WEATHER_FAILED,
        };

        expect(getWeatherFailed()).toEqual(exceptedObject);
      });
    });
  });

  describe("async functions", () => {
    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);
    const initialState = {
      weather: {
        fetchingWeather: false,
        weather: {},
      },
    };
    const store = mockStore(initialState);

    afterEach(() => {
      store.clearActions();
    });

    describe("getWeather", () => {
      it("success without save", async () => {
        mockAxios.get.mockImplementation(() =>
          Promise.resolve({ data: weatherMock })
        );

        const expectedActions = [getWeatherSuccess()];

        await store.dispatch(getWeather("Warsaw", false));
        expect(store.getActions()).toEqual(expectedActions);
      });

      it("success with save", async () => {
        mockAxios.get.mockImplementation(() =>
          Promise.resolve({ data: weatherMock })
        );

        const expectedActions = [
          getWeatherRequest(),
          getWeatherSuccessWithSave(weatherMock),
        ];

        await store.dispatch(getWeather("Warsaw", true));
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
