import axios from "axios";
import {
  GET_WEATHER_FAILED,
  GET_WEATHER_REQUEST,
  GET_WEATHER_SUCCESS,
  GET_WEATHER_WITHOUT_SAVE_SUCCESS,
} from "../constants/weather";

export const getWeatherRequest = () => ({
  type: GET_WEATHER_REQUEST,
});

export const getWeatherSuccess = () => ({
  type: GET_WEATHER_WITHOUT_SAVE_SUCCESS,
});

export const getWeatherSuccessWithSave = (data) => ({
  type: GET_WEATHER_SUCCESS,
  payload: data,
});

export const getWeatherFailed = () => ({
  type: GET_WEATHER_FAILED,
});

/**
 * Get weather for city
 *
 * @param {string} city - name city
 * @param {boolean} saveData - option to save data into redux
 */
export const getWeather = (city, saveData) => (dispatch) =>
  new Promise(async (resolve, reject) => {
    if (saveData) dispatch(getWeatherRequest());

    try {
      const { data } = await axios.get(
        `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${process.env.REACT_APP_WEATHER_APP_KEY}`
      );

      if (saveData) {
        dispatch(getWeatherSuccessWithSave(data));
      } else {
        dispatch(getWeatherSuccess());
      }

      resolve(data);
    } catch (err) {
      dispatch(getWeatherFailed());
      reject(err);
    }
  });
