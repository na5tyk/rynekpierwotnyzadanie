import React, { useState } from "react";
import { useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { Row, Col, Card } from "react-bootstrap";

import { getWeather } from "../../store/actions/weatherActions";

const CompareWeather = (props) => {
  const dispatch = useDispatch();

  const [first, setFirst] = useState(null);
  const [second, setSecond] = useState(null);
  const [third, setThird] = useState(null);

  React.useEffect(() => {
    if (props.city.name !== "Warsaw")
      dispatch(getWeather("Warsaw")).then((r) => {
        setFirst(r);
      });

    if (props.city.name !== "Krakow")
      dispatch(getWeather("Krakow")).then((r) => {
        setSecond(r);
      });

    if (props.city.name !== "Gdynia")
      dispatch(getWeather("Gdynia")).then((r) => {
        setThird(r);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const preparedTemp = (city) => {
    const temp = Math.abs((props.city.main.temp - city.main.temp).toFixed(2));

    if (props.city.main.temp > city.main.temp) {
      return `Temperatura w ${props.city.name} jest większa o ${temp} stopni niż w ${city.name}`;
    }

    if (props.city.main.temp < city.main.temp) {
      return `Temperatura w mieście ${props.city.name} jest mniejsza o ${temp} stopni niż w ${city.name}`;
    }

    return `Temperatura w  mieście ${props.city.name} jest taka sama jak w ${first.main.name}.`;
  };

  const preparedHumidity = (city) => {
    const temp = Math.abs(
      (props.city.main.humidity - city.main.humidity).toFixed(2)
    );

    if (props.city.main.humidity > city.main.humidity) {
      return `Wilgotność w ${props.city.name} jest większa o ${temp}% niż w ${city.name}`;
    }

    if (props.city.main.humidity < city.main.humidity) {
      return `Wilgotność w mieście ${props.city.name} jest mniejsza o ${temp}% niż w ${city.name}`;
    }

    return `Wilgotność w  mieście ${props.city.name} jest taka sama jak w ${first.main.name}.`;
  };

  return (
    <Card className="mt-3">
      <Row>
        {first && (
          <Col>
            <p className="text-center">{preparedTemp(first)}</p>
            <p className="text-center">{preparedHumidity(first)}</p>
          </Col>
        )}
        {second && (
          <Col>
            <p className="text-center">{preparedTemp(second)}</p>
            <p className="text-center">{preparedHumidity(second)}</p>
          </Col>
        )}
        {third && (
          <Col>
            <p className="text-center">{preparedTemp(third)}</p>
            <p className="text-center">{preparedHumidity(third)}</p>
          </Col>
        )}
      </Row>
    </Card>
  );
};

CompareWeather.propTypes = {
  city: PropTypes.object.isRequired,
};

export default CompareWeather;
