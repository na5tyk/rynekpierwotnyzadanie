import React from "react";
import { shallow } from "enzyme";

import CompareWeather from "./index";
import { weatherMock } from "../../test_mocks/weatherMocks";

const mockDispatch = jest.fn();

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useDispatch: () => mockDispatch,
}));

const component = shallow(<CompareWeather city={weatherMock} />);

describe("CompareWeather", () => {
  it("should render without error", () => {
    expect(component).toBeTruthy();
  });

  it("snapshot", () => {
    expect(component).toMatchSnapshot();
  });
});
