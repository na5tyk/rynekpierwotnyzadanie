import React from "react";
import { shallow } from "enzyme";

import SearchWeatherForm from "./index";

const mockDispatch = jest.fn();
const mockHistoryPush = jest.fn();

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useDispatch: () => mockDispatch,
}));

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

const component = shallow(<SearchWeatherForm />);

describe("SearchWeatherForm", () => {
  it("should render without error", () => {
    expect(component).toBeTruthy();
  });

  it("snapshot", () => {
    expect(component).toMatchSnapshot();
  });

  it("Label should have correct text", () => {
    const label = component.find("InputGroupText");
    expect(label.text()).toBe("Miasto");
  });

  describe("input", () => {
    const formControl = component.find("FormControl");

    it("should have correct values", () => {
      expect(formControl.props().name).toBe("city");
      expect(formControl.props().placeholder).toBe("Wpisz miasto");
    });
  });

  describe("button", () => {
    const button = component.find("Button");

    it("should have correct values", () => {
      expect(button.text()).toBe("Szukaj");
      expect(button.prop("variant")).toBe("outline-secondary");
      expect(button.prop("active")).toBe(false);
      expect(button.prop("disabled")).toBe(false);
    });

    it("should call function after click", () => {
      button.simulate("click");
      expect(mockHistoryPush).toHaveBeenCalled();
    });
  });
});
