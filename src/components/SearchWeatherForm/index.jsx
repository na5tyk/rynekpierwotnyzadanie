import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Row, Col, InputGroup, FormControl, Button } from "react-bootstrap";

const SearchWeatherForm = () => {
  const history = useHistory();
  const [city, setCity] = useState();

  const onSubmit = () => {
    history.push(`/${city}`);
  };

  return (
    <Row className="mt-3">
      <Col>
        <InputGroup size="sm" className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="inputGroup-sizing-sm">Miasto</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            name="city"
            onChange={(e) => setCity(e.target.value)}
            onKeyPress={(e) => (e.charCode === 13 ? onSubmit() : null)}
            placeholder="Wpisz miasto"
          />
          <InputGroup.Append>
            <Button variant="outline-secondary" onClick={onSubmit}>
              Szukaj
            </Button>
          </InputGroup.Append>
        </InputGroup>
      </Col>
    </Row>
  );
};

export default SearchWeatherForm;
