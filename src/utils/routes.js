const routes = {
  homeView: "/",
  weatherView: "/:city",
  notFound: "*",
};

export default routes;
