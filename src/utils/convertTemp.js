export const convertTemp = (temp) => {
  if (!temp || typeof temp !== "number") return "-";

  temp = temp - 273.15;
  return `${temp.toFixed(2)} °C`;
};
