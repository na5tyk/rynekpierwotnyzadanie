import { convertTemp } from "./convertTemp";

describe("convertTemp", () => {
  test("should return correct value when parametr isn't number or is empty", () => {
    expect(convertTemp("152")).toBe("-");
    expect(convertTemp(null)).toBe("-");
    expect(convertTemp(undefined)).toBe("-");
    expect(convertTemp({})).toBe("-");
    expect(convertTemp([])).toBe("-");
  });

  test("should return correct value when parametr is number", () => {
    expect(convertTemp(300)).toBe("26.85 °C");
  });
});
