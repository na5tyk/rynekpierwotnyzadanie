# Instalacja projektu

Należy zmienić nazwę pliku `.exmaple.env` na `.env` i uzupełnić wartością.

Przykładowy klucz dla OpenWeatherMap: `070e9b8040a40c860e803985e6d99827`

```
npm install
```

# Uruchomienie serwera

```
npm start
```

# Uruchomienie testów

```
npm test
```

lub z wynikiem coverage:

```
npm test -- --coverae
```
